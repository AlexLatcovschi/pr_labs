# Laboratory 1

## Mandatory Tasks
1. Pull a docker container (alexburlacu/pr-server) from the registry
2. Run it, don't forget to forward the port 5000 to the port that you want on the local machine
3. Only languages and libraries supporting threads, locks and semaphores are allowed. Node or JS generally, Go, Elixir/Erlang are prohibited.
4. Now that you're up and running, you need to access the root route of the server and find your way to register
5. The access token that you get after accessing the register route must be put in http header of subsequent requests under the key X-Access-Token key
6. Most routes return a json with data and link keys. Extract data from data key and get next links fron link key
7. Hardcoding the routes is strictly forbidden. You need to "traverse" the api
8. Access token has a timeout of 20 seconds, and you are not allowed to get another token every time you access different route. So, one register per program run
9. Once you fetch all the data, convert it to a common representation, doesn't matter what this representation is
10. The final part of the lab is to make a concurrent TCP server, serving the fetched content, that will respond to (mandatory) a column selector message, like `SelectColumn column_name`, and (optional) `SelectFromColumn column_name glob_pattern`
11. All the code must be on GitHub with a readme file explaining the task and implementation

## Optional tasks:
- `SelectFromColumn column_name glob_pattern` message for TCP server
- Implementing your own thread pools
- Using a limited number of threads for fetching data
- Dispatch data to a different process pool/dedicated processes for conversion
- Anything else, turn on your imagination

## Grading policy:
-1 for badly written code (formatting, coupling, alignment with language conventions)
-3 (more than 50% of the lab is plagiarized)
-1 (less than 50% of the lab is plagiarized)
-1 (you can't explain how the code works or why you made some decision)
- if the required functionality is not implement, in the required way, lab is invalid - no grade
- if you are late one week, you are subtracted one point. Still, you can recover it by implementing some optional features

# Implementation
###Use docker commands:
-docker tag alexburlacu/pr-server YOUR_USER_NAME/localhost:5000
-docker push YOUR_USER_NAME/localhost:5000
-docker run -d -p 5000:5000 YOUR_USER_NAME/localhost:5000
-open http://localhost:5000/register
-open new console and enable telnet on your OS
-run (telnet localhost 5080)
### Developed on Java


