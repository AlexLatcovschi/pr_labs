import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Main {

    private static ArrayList<String> links = new ArrayList<String>();
    private static final String initialURl = "http://localhost:5000/home";
    private static int index = 0;
    private static ArrayList<String> dataList = new ArrayList<>();
    private static volatile int nrOfWorkingThreads = 0;
    private static final int serverPort = 5080;

    public ArrayList<String> getDataList() {
        return dataList;
    }

    public static void main(String[] args) {

        Token requestAccessToken = new Token();
        requestAccessToken.requstAccessToken();
        long startTime = System.currentTimeMillis();
        new ThreadRequest(initialURl).start();
        while (true) {
            if (nrOfWorkingThreads == 0) {
                long stopTime = System.currentTimeMillis();
                long elapsedTime = stopTime - startTime;
                System.out.println("Execution time milliseconds = " + elapsedTime + "\nEnter in telnet port " + serverPort);
                TCPServer tcpServer = new TCPServer();
                tcpServer.start(serverPort);
                break;
            }
        }
    }

    static class ThreadRequest extends Thread {
        private String url;

        ThreadRequest(String url) {
            this.url = url;
            nrOfWorkingThreads++;
        }

        @Override
        public void run() {
            Data requestData = new Data();
            Token requestAccessToken = new Token();

            String result = "";
            int code = 200;
            try {
                URL siteURL = new URL(url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) siteURL.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("X-Access-Token", requestAccessToken.getAccessToken());
                httpURLConnection.setConnectTimeout(3000);
                httpURLConnection.connect();

                code = httpURLConnection.getResponseCode();
                if (code == 200) {
                    String data = requestData.readData(httpURLConnection);
                    if (requestData.getLinks(data) != null) {
                        links.addAll(requestData.getLinks(data));
                    }

                    System.out.println(url + "Status:" + code + "     type = " + requestData.getType(data));
                    if (requestData.getData(data) != null) {
                        convert(requestData.getType(data), requestData.getData(data));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (links != null) {
                for (int i = index; i < links.size(); i++) {
                    new ThreadRequest(links.get(i)).start();
                    index++;
                }
            }
            nrOfWorkingThreads--;
        }

        void convert(String type, String data) {
            Convertor convertor = new Convertor();
            switch (type) {

                case "application/xml": {
                    dataList.add(convertor.convertXMLtoJSON(data));
                    break;
                }

                case "text/csv": {
                    dataList.add(convertor.convertCSVtoJSON(data));
                    break;
                }

                case "application/x-yaml": {
                    dataList.add(convertor.convertYamlToJson(data));
                    break;
                }
                case "json": {
                    dataList.add(data);
                    break;
                }
                default:
                    System.out.println("default type");
                    break;
            }
        }
    }
}
