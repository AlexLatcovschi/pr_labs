import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;

class TCPServer {
    private static final String telnetMenu = " \nEnter column key to see this (ex: id) \n Enter key value to see this(ex: id 10)";

    public void start(int port) {
        try {
            Parser parseJson = new Parser();
            Main main = new Main();

            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server is listening on port " + port);

            Socket clientSocket = serverSocket.accept();
            System.out.println("Connection accepted!");

            PrintWriter print = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            ArrayList<String> arrayList = main.getDataList();
            ArrayList<String> output;
            String inputLine;
            boolean exit = true;
            print.println(telnetMenu);

            while ((inputLine = in.readLine()) != null && exit) {
                ArrayList<String> words = new ArrayList<>(Arrays.asList(inputLine.split(" ")));

                switch (words.size()) {
                    case 1: {
                        if ("exit".equals(inputLine)) {
                            print.println("Good Bye");
                            exit = false;
                        } else {
                            output = parseJson.getValuesForGivenKey(arrayList, inputLine);
                            for (String outputElement : output) {
                                print.println(outputElement);
                            }
                            print.println(telnetMenu);
                        }
                        break;
                    }
                    case 2: {
                        output = parseJson.getJsonForGivenValue(arrayList, words.get(0), words.get(1));
                        for (String outputElement : output) {
                            print.println(outputElement);
                        }
                        print.println(telnetMenu);
                        break;
                    }
                    default: {
                        print.println("Unknown command! Try one from:");
                        print.println(telnetMenu);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}