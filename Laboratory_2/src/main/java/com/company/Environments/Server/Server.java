package com.company.Environments.Server;
import com.company.UDPProtocol.ServerUDP;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class Server extends Thread {


    public static void main(String[] args) {
        // set server port
        int serverPort = 5432;
        InetAddress inetAddress = null;

        try {
            inetAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        ServerUDP server = new ServerUDP(inetAddress, serverPort);
        // run server
        try {
            new Send(server).start();
            new Receive(server).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

