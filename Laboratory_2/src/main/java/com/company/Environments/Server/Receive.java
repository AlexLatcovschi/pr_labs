package com.company.Environments.Server;

import com.company.UDPProtocol.ServerUDP;

public class Receive extends Thread {
    ServerUDP serverUDP;

    Receive(ServerUDP serverUDP) {
        this.serverUDP = serverUDP;
    }

    public void run() {
        while (true) {
            try {
                serverUDP.broadcast(serverUDP.receiveWithRetransmission());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


