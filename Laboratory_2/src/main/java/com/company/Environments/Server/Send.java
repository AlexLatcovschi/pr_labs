package com.company.Environments.Server;

import com.company.UDPProtocol.ServerUDP;

import java.util.Scanner;

public class Send extends Thread {
    ServerUDP serverUDP;
    Send(ServerUDP serverUDP) {
        this.serverUDP = serverUDP;
    }

    @Override
    public void run() {
        String userInput;

        while (true) {
            userInput = new Scanner(System.in).nextLine();
            try {
                serverUDP.sendMessageToAllClients(userInput);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
