package com.company.Environments.Client;

import com.company.UDPProtocol.ClientUDP;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) {
        // set app ports
        int serverPort = 5432;
        int clientPort;
        clientPort = 4000;
        InetAddress inetAddress = null;
        // set inet location
        try {
            inetAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        ClientUDP client = new ClientUDP(inetAddress, serverPort, clientPort);
        try {
            client.init();
            Send clientSendThread = new Send(client);
            Receive clientReceiveThread = new Receive(client);
            clientSendThread.start();
            clientReceiveThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
