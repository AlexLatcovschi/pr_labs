package com.company.Environments.Client;

import com.company.UDPProtocol.ClientUDP;

import java.util.Scanner;

public class Send extends Thread {
    ClientUDP client;

    Send(ClientUDP client) {
        this.client = client;
    }

    @Override
    public void run() {
        String userInput;
        while (true) {
            userInput = new Scanner(System.in).nextLine();
            try {
                if (userInput.equals("end")) {
                    client.disconnect();
                } else
                    client.sendPacket(userInput);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
