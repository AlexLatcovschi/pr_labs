package com.company.Environments.Client;

import com.company.UDPProtocol.ClientUDP;

public class Receive extends Thread {
    ClientUDP client;

    Receive(ClientUDP client) {
        this.client = client;
    }

    @Override
    public void run() {
        while (true) {
            try {
                client.receiveWithRetransmission();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
