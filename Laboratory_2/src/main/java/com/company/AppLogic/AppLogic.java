package com.company.AppLogic;

import com.company.Encryption.AdvancedEncryptionStandart;
import com.company.Encryption.RSAAlgorithm;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.SecretKey;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class AppLogic {
    public String createTransmissionPacket(String message, PrivateKey privateKey, SecretKey secretKey) throws Exception {
        return AdvancedEncryptionStandart.encrypt(new JSONObject().put("message", message).put("checkSum", RSAAlgorithm.sign(message, privateKey)).toString(), secretKey, AdvancedEncryptionStandart.getCipher());
    }

    public String createConnection(String status, PublicKey publicKey) throws Exception {
        return new JSONObject().put("status", status).put("publicKey", Base64.getEncoder().encodeToString(publicKey.getEncoded())).toString();
    }

    public String createRetransmission(String retransmission) throws Exception {
        return new JSONObject().put("retransmission", retransmission).toString();
    }


    public String getMessage(String json, SecretKey secretKey) throws Exception {
        return new JSONObject(json).get("message").toString();
    }

    public String getCheckSum(String json) throws JSONException {
        return new JSONObject(json).get("checkSum").toString();
    }

    public PublicKey getPublicKey(String json) throws JSONException, InvalidKeySpecException, NoSuchAlgorithmException {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(new JSONObject(json).get("publicKey").toString())));
    }

    public boolean isValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
