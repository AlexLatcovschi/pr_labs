package com.company.UDPProtocol;

import com.company.AppLogic.AppLogic;
import com.company.Encryption.AdvancedEncryptionStandart;
import com.company.Encryption.RSAAlgorithm;
import org.json.JSONObject;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

public class ClientUDP {
    private DatagramSocket datagramSocket;
    private final InetAddress ip;
    private final int serverPort;
    private String buffer;
    private DatagramPacket datagramPacket;
    private static final PublicKey clientPublicKey;
    private static final PrivateKey clientPrivateKey;
    private static PublicKey serverPublicKey;
    private static final SecretKey secretKey;
    private long startConnectionTime;
    private long endConnectionTime;
    private long messageSendTime;
    private long messageResponseTime;
    private boolean receive;


    static {
        RSAAlgorithm.generateKeyPair();
        AdvancedEncryptionStandart.generateAES();
        clientPublicKey = RSAAlgorithm.getPublicKey();
        clientPrivateKey = RSAAlgorithm.getPrivateKey();
        secretKey = AdvancedEncryptionStandart.getSecretKey();
    }

    public ClientUDP(InetAddress ip, int serverPort, int clientPort) {
        try {
            this.datagramSocket = new DatagramSocket(clientPort);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        this.ip = ip;
        this.serverPort = serverPort;
    }

    public void init() throws Exception {
        AppLogic jsonLogic = new AppLogic();
        send(jsonLogic.createConnection("Connecting", clientPublicKey));
        startConnectionTime = System.nanoTime();
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        if (TimeUnit.NANOSECONDS.toMillis(endConnectionTime - startConnectionTime) == TimeUnit.NANOSECONDS.toMillis(-startConnectionTime)) {
                            System.out.println("Can't connect to server");
                            System.exit(0);
                        } else if (!(endConnectionTime == 0)) {
                            System.out.println("Connected to server: " + ip + ":" + serverPort);
                        }
                    }
                },
                2000
        );

        String response = receive();
        if (jsonLogic.isValid(response)) {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("status")) {
                if (jsonObject.get("status").toString().equals("Connected")) {
                    serverPublicKey = jsonLogic.getPublicKey(response);
                    send(sendAESKey(secretKey, serverPublicKey));
                }
            }
        }
        endConnectionTime = System.nanoTime();
    }

    public void disconnect() throws Exception {
        send(disconnection());
        System.out.println("Disconnected");
        System.exit(0);
    }

    public void send(String message) throws IOException {
        byte[] sendBuffer = message.getBytes();
        datagramPacket = new DatagramPacket(sendBuffer, sendBuffer.length, ip, serverPort);
        datagramSocket.send(datagramPacket);
    }


    public void sendPacket(String message) throws Exception {
        send(createPacket(message));
        messageSendTime = System.nanoTime();
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        if (TimeUnit.NANOSECONDS.toMillis(messageResponseTime - messageSendTime) == TimeUnit.NANOSECONDS.toMillis(-messageSendTime)) {
                            System.out.println("Can't reach server");
                            System.exit(0);
                        } else if (!receive) {
                            System.out.println("An error occurred!");
                            System.exit(0);
                        }
                    }
                },
                2000
        );
        receive = false;
    }

    public String createPacket(String message) throws Exception {
        buffer = message;
        return new AppLogic().createTransmissionPacket(message, clientPrivateKey, secretKey);
    }

    public String receive() throws IOException {
        byte[] receiveBuffer = new byte[4000];
        datagramPacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
        datagramSocket.receive(datagramPacket);
        messageResponseTime = System.nanoTime();
        receive = true;
        return new String(receiveBuffer).trim();
    }


    public void receiveWithRetransmission() throws Exception {
        AppLogic jsonLogic = new AppLogic();
        String receive = receive();
        if (!jsonLogic.isValid(receive)) {
            receive = AdvancedEncryptionStandart.decrypt(receive, secretKey, AdvancedEncryptionStandart.getCipher());
        }
        JSONObject jsonObject = new JSONObject(receive);
        if (jsonObject.has("retransmission")) {
            if (jsonObject.get("retransmission").toString().equals("true")) {
                sendPacket(buffer);
                System.out.println(buffer);
            }
        } else if (jsonObject.has("checkSum")) {
            String message = jsonLogic.getMessage(receive, secretKey);
            if (RSAAlgorithm.verify(message, jsonLogic.getCheckSum(receive), serverPublicKey)) {
                send(jsonLogic.createRetransmission("false"));
                System.out.println(message);
            } else {
                send(jsonLogic.createRetransmission("true"));
            }
        }
    }

    public String sendAESKey(SecretKey secretKey, PublicKey publicKey) throws Exception {
        return new JSONObject().put("secretKey", RSAAlgorithm.encrypt(Base64.getEncoder().encodeToString(secretKey.getEncoded()), publicKey)).toString();
    }

    public String disconnection() throws Exception {
        return new JSONObject().put("disconnected", "disconnected").toString();
    }
}
