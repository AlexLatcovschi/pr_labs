package com.company.UDPProtocol;

import com.company.AppLogic.AppLogic;
import com.company.Encryption.AdvancedEncryptionStandart;
import com.company.Encryption.RSAAlgorithm;
import org.json.JSONObject;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class ServerUDP {
    private DatagramSocket datagramSocket;
    private final InetAddress ip;
    private DatagramPacket datagramPacket;
    private static int port;
    private static final PublicKey serverPublicKey;
    private static final PrivateKey serverPrivateKey;
    private static PublicKey clientPublicKey;
    private String buffer;
    private final HashMap<Integer, PublicKey> portPublicKeyClientHashMap = new HashMap<Integer, PublicKey>();
    private final HashMap<Integer, SecretKey> portSecretKeyeyClientHashMap = new HashMap<Integer, SecretKey>();
    private SecretKey secretKey;

    static {
        RSAAlgorithm.generateKeyPair();
        serverPublicKey = RSAAlgorithm.getPublicKey();
        serverPrivateKey = RSAAlgorithm.getPrivateKey();
    }

    public ServerUDP(InetAddress ip, int serverPort) {
        try {
            this.datagramSocket = new DatagramSocket(serverPort);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        this.ip = ip;
    }

    public void acceptConnection(String receive) throws Exception {
        AppLogic jsonLogic = new AppLogic();
        port = datagramPacket.getPort();
        JSONObject jsonObject = new JSONObject(receive);
        if (jsonObject.get("status").toString().equals("Connecting")) {
            clientPublicKey = jsonLogic.getPublicKey(receive);
            portPublicKeyClientHashMap.put(port, clientPublicKey);
            send(jsonLogic.createConnection("Connected", serverPublicKey), port);
            System.out.println("New user connected: " + datagramPacket.getAddress() + ":" + datagramPacket.getPort());
        }
    }

    public void getSecretKey(String receive) throws Exception {
        secretKey = getAESKey(receive, serverPrivateKey);
        portSecretKeyeyClientHashMap.put(port, secretKey);
    }


    public void send(String message, int port) throws IOException {
        datagramPacket = new DatagramPacket(message.getBytes(), message.getBytes().length, ip, port);
        datagramSocket.send(datagramPacket);
    }

    public void sendPacket(String message, int port, SecretKey secretKey) throws Exception {
        send(createPacket(message, secretKey), port);
    }

    public String createPacket(String message, SecretKey secretKey) throws Exception {
        buffer = message;
        return new AppLogic().createTransmissionPacket(message, serverPrivateKey, secretKey);
    }

    public String receive() throws IOException {
        byte[] receiveBuffer = new byte[4000];
        datagramPacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
        datagramSocket.receive(datagramPacket);
        port = datagramPacket.getPort();
        clientPublicKey = portPublicKeyClientHashMap.get(port);
        secretKey = portSecretKeyeyClientHashMap.get(port);
        return new String(receiveBuffer).trim();
    }


    public String receiveWithRetransmission() throws Exception {
        AppLogic jsonLogic = new AppLogic();
        String receive = receive();
        if (!jsonLogic.isValid(receive)) {
            receive = AdvancedEncryptionStandart.decrypt(receive, secretKey, AdvancedEncryptionStandart.getCipher());
        }
        JSONObject jsonObject = new JSONObject(receive);
        if (jsonObject.has("checkSum")) {
            String message = jsonLogic.getMessage(receive, secretKey);
            if (RSAAlgorithm.verify(message, jsonLogic.getCheckSum(receive), clientPublicKey)) {
                if (message.equals("get clients")) {
                    String clientsPorts = "";
                    for (Map.Entry<Integer, PublicKey> entry : portPublicKeyClientHashMap.entrySet()) {
                        int port = entry.getKey();
                        clientsPorts = clientsPorts + port + " ";
                        System.out.println(clientsPorts);
                    }
                    System.out.println(clientsPorts);
                    sendPacket(clientsPorts, port, secretKey);
                    message = "";
                }
                System.out.println("Received message: " + message);
                send(jsonLogic.createRetransmission("false"), port);
            } else {
                send(jsonLogic.createRetransmission("true"), port);
            }
        } else if (jsonObject.has("retransmission")) {
            if (jsonObject.get("retransmission").toString().equals("true")) {
                sendPacket(buffer, port, secretKey);
            }
        } else if (jsonObject.has("status")) {
            acceptConnection(receive);
        } else if (jsonObject.has("disconnected")) {
            System.out.println("Client: " + datagramPacket.getAddress() + ":" + datagramPacket.getPort() + " disconnected");
            portPublicKeyClientHashMap.remove(port);
            portSecretKeyeyClientHashMap.remove(port);
        } else if (jsonObject.has("secretKey")) {
            getSecretKey(receive);
        }
        return receive;
    }

    public void broadcast(String message) throws Exception {
        AppLogic jsonLogic = new AppLogic();
        int senderPort = port;
        if (jsonLogic.isValid(message)) {
            JSONObject jsonObject = new JSONObject(message);
            if (jsonObject.has("message")) {
                message = jsonLogic.getMessage(message, secretKey);
                if (!portPublicKeyClientHashMap.isEmpty()) {
                    if (message.contains("port")) {
                        port = Integer.parseInt(message.substring(message.indexOf("port") + 5));
                        secretKey = portSecretKeyeyClientHashMap.get(port);
                        clientPublicKey = portPublicKeyClientHashMap.get(port);
                        message = message.substring(0, message.indexOf("port:"));
                        sendPacket(message, port, secretKey);
                    } else if (message.contains("get clients")) {
                    } else {
                        for (Map.Entry<Integer, PublicKey> entry : portPublicKeyClientHashMap.entrySet()) {
                            if (entry.getKey() == senderPort)
                                continue;
                            port = entry.getKey();
                            secretKey = portSecretKeyeyClientHashMap.get(port);
                            clientPublicKey = entry.getValue();
                            sendPacket(senderPort + ": " + message, port, secretKey);
                        }
                    }
                }
            }
        }
    }

    public void sendMessageToAllClients(String message) throws Exception {
        if (!portPublicKeyClientHashMap.isEmpty()) {
            for (Map.Entry<Integer, PublicKey> entry : portPublicKeyClientHashMap.entrySet()) {
                port = entry.getKey();
                secretKey = portSecretKeyeyClientHashMap.get(port);
                clientPublicKey = entry.getValue();
                sendPacket(message, port, secretKey);
            }
        } else {
            System.out.println("No user connected");
            System.exit(0);
        }
    }

    public SecretKey getAESKey(String encodedKey, PrivateKey privateKey) throws Exception {
        byte[] decodedKey = Base64.getDecoder().decode(RSAAlgorithm.decrypt(new JSONObject(encodedKey).get("secretKey").toString(), privateKey));
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }
}


