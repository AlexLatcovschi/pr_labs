####You are tasked with implementing a protocol stack, namely a transport protocol based on UDP, a session-level security protocol inspired by SSL/TLS, and an application-level protocol. You must present this project as a client and a server, both using a library that contains the protocol logic. The library must be made up of 3 modules, for each level of the protocol stack, with a well-defined API and that adheres to the layered architecture. For transport and session level protocols the BSD Sockets API is a recommended source of inspiration, while for the application-level protocol something that resembles an HTTP client API is a recommended source of inspiration.

#Now, for the technicalities:
 Implement a protocol atop UDP, with error checking and retransmissions. Limit the number of retries for retransmission.
 Make the connection secure, using either a CA to get the public key of the receiver and encrypt data with it, or using Diffie-Helman to get a shared connection key between client and server, ensure that the traffic is encrypted.

#Regarding the application-level protocol, you have 3 options:
  - make an FTP-like protocol for data transfer, thus you will need to ensure data splitting and in-order delivery and reassembly at the destination. The protocol must support URIs, file creation and update (PUT), file fetching (GET) and metadata retrieval (OPTIONS)
  - make a protocol based on the workings (state machine) of an ATM
  - make a protocol based on the workings (state machine) of a stationary telephone
#All the stuff outlined above is for at most mark 8. To get to 10, you must also implement some bonus tasks (the more the merrier).

##A suggestive list of bonus tasks:
Error correction codes at the transport level, instead of error checking and retransmission
In order delivery (available as a bonus task if application-level protocol isn't FTP-like)
Congestion/Flow control, to ensure Quality of Service
Actual QoS, using packet priorities (research on your own)
The client and server are using epoll (or IOCP on Windows)
Streaming compression at the transport level
Think for yourself of something else

#To run project: (assure that ports 4000 && 5432 are free)
- run client environment and it will open on port 4000; {here we use UDP protocol for receiving data
with retransmission and throwing errors, also we can send a packet to server, with logics
in Send.Java}
-- (got to src/main/java/com/company/environments/Client/Client.java and run)
- run server on port 5432; {here we have a broadcast to send data to all clients,
                            also retransmission in case of errors && eroor checking}
-- (got to src/main/java/com/company/environments/Server/Server.java and run)
- for security in data sending and receiving, we use AES (We use it for retransmission -> Advanced Encryption Standart is a symmetric block cipher
. AES includes three block ciphers: AES-128, AES-192 and AES-256.
  AES-128 uses a 128-bit key length to encrypt and decrypt a block of messages, while AES-192 uses a 192-bit key length and AES-256 a 256-bit key length to encrypt and decrypt messages. Each cipher encrypts and decrypts data in blocks of 128 bits using cryptographic keys of 128, 192 and 256 bits, respectively.
  )
- for encrypting and decrypting we use RSA algorithm (Rivest–Shamir–Adleman - It is an asymmetric cryptographic algorithm. Asymmetric means that there are two different keys. This is also called public key cryptography, because one of the keys can be given to anyone, we generate this keys in AppLogic, and use in ClientUdp and ServerUdp)
- for better delivery we use Datagram Packet (Datagram packets are used to implement a connectionless packet delivery service. Each message is routed from one machine to another based solely on information contained within that packet. Multiple packets sent from one machine to another might be routed differently, and might arrive in any order. Packet delivery is not guaranteed.)
## As a not required aspects:
- implement timeout connection
- connection messages
- broadcast sending
 